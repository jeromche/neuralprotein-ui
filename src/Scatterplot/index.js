import React, { Component } from 'react';
import { observer } from 'mobx-react';
import C3Chart from '../components/C3Chart';
import LoadingIndicator from '../components/LoadingIndicator';
import 'c3/c3.css';
import './style.css';

@observer(['neighbors', 'cath'])
export default class ScatterPlot extends Component {
  config() {
    const cath = this.props.cath;

    if (cath.isLoading) return;

    const embedding2d = this.props.neighbors.embedding2D;
    const mapping = cath.mapping;
    const positions2d = cath.positions2d;
    const xs = { embedding: 'embedding-X' };
    const classes = {};
    const labels = { embedding: 'Nearest Neighbor' };
    const columns = [];

    for (let item of positions2d) {
      let label = 'class-' + item.class;

      if ( ! classes.hasOwnProperty(label + '-X')) {
        classes[label + '-X'] = [];
        classes[label + '-Y'] = [];

        xs[label + '-Y'] = label + '-X';

        labels[label + '-Y'] = mapping[item.class];
      }

      classes[label + '-X'].push(item.x);
      classes[label + '-Y'].push(item.y);
    }

    for (let className in classes) if (true) {
      columns.push([className, ...classes[className]]);
    }

    columns.push(
      ['embedding', embedding2d.y],
      ['embedding-X', embedding2d.x],
    );

    const config = {
      data: {
        type: 'scatter',
        xs: xs,
        columns: columns,
        names: labels,
        colors: {
          embedding: 'black',
        },
        classes: {
          embedding: 'embedding',
        },
      },
      legend: { show: false, },
      axis: {
        x: { show: false },
        y: { show: false },
      },
    };

    return config
  }

  render() {
    if (this.props.cath.isLoading) {
      return <LoadingIndicator name="Scatterplot"/>
    }

    const config = this.config();
    return <C3Chart config={config}/>
  }
}
