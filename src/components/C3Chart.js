import React, {Component, PropTypes} from 'react'
import LoadingIndicator from './LoadingIndicator'
import c3 from 'c3'

export default class Chart extends Component {
  static propTypes = {
    config: PropTypes.object,
  }

  componentDidMount() {
    this.generateChart()
  }

  componentWillUnmount() {
    this.chart = this.chart.destroy()
  }

  generateChart() {
    const options ={
      bindto: this.element,
      ...this.props.config,
    };

    window.setTimeout(() => {
      this.chart = c3.generate(options)
    }, 100)
  }

  render() {
    return (
      <div ref={(element) => { this.element = element }}>
        <LoadingIndicator name="Scatterplot"/>
      </div>
    )
  }
}
