import React from 'react';

export default class ImageCarousel extends React.Component {
  constructor() {
    super();
    this.state = { i: 0 };
  }

  next(event) {
    event.preventDefault();
    var i = this.state.i + 1;
    if (i > this.props.images.length) {
      i = 0;
    }
    this.setState({ i: i });
  }

  previous(event) {
    event.preventDefault();
    var i = this.state.i - 1;
    if (i < 0) {
      i = this.props.images.length - 1;
    }
    this.setState({ i: i });
  }

  render() {
    const images = this.props.images;

    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-xs-1">
            <a href="#" onClick={this.previous.bind(this)}>◀</a>
          </div>
          <div className="col-xs-5 col-sm-7 col-md-9">
            <a href={'http://' + images[this.state.i]} target="_blank">
              <img src={'http://' + images[this.state.i]} className="img-fluid" role="presentation"/>
            </a>
          </div>
          <div className="col-xs-1">
            <a href="#" onClick={this.next.bind(this)}>▶</a>
          </div>
        </div>
      </div>
    );
  }
}
