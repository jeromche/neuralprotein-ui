import React from 'react'
import { Link } from 'react-router'

export default class NavLink extends Link {
  contextTypes: {
    router: React.PropTypes.object
  }

  render() {
    let isActive = this.context.router.isActive(this.props.to, true);
    let className = 'nav-link';

    if (isActive) {
      className += ' active';
    }

    return (
      <li className="nav-item">
        <Link {...this.props} className={className}>
          {this.props.children}
        </Link>
      </li>
    )
  }
}
