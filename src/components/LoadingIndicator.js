import React from 'react'

const LoadingIndicator = ({ name }) => (
  <div className="row">
    <div className="col-md-12">
      <p>Loading {name}</p>
    </div>
  </div>
)

export default LoadingIndicator
