import React from 'react';
import ImageCarousel from './ImageCarousel';

class ClosestDomainsItem extends React.Component {
  constructor() {
    super();
    this.state = { showMore: false };
  }

  open(event) {
    event.preventDefault();
    this.setState({ showMore: true });
  }

  close(event) {
    event.preventDefault();
    this.setState({ showMore: false });
  }

  prependHTTP(url) {
    var prefix = 'http://';
    if (url.substr(0, prefix.length) !== prefix) {
      url = prefix + url;
    }
    return url;
  }

  render() {
    const domain = this.props.domain;

    return (
      <tbody>
        <tr>
          <td>{domain.domainId}</td>
          <td>{domain.name}</td>
          <td>{domain.classification}</td>
          <td>
            {!this.state.showMore &&
              <a href="#" onClick={this.open.bind(this)}>open</a>
            }

            {this.state.showMore &&
              <a href="#" onClick={this.close.bind(this)}>close</a>
            }
          </td>
        </tr>

        {this.state.showMore &&
          <tr>
            <td colSpan="4">
              <div className="row">
                <div className="col-sm-8">
                  <h6>External Links</h6>

                  {typeof domain.externalLinks === 'string' &&
                    <a href={this.prependHTTP(domain.externalLinks)} target="blank">
                      {domain.externalLinks}
                    </a>
                  }

                  {typeof domain.externalLinks === 'object' &&
                    <ul>
                      {domain.externalLinks.map(function(url, i)  {
                        return (
                          <li key={i}>
                            <a href={this.prependHTTP(url)} target="blank">{url}</a>
                          </li>
                        );
                      }.bind(this))}
                    </ul>
                  }
                </div>

                <div className="col-sm-4">
                  <ImageCarousel images={domain.imageUrls}/>
                </div>
              </div>
            </td>
          </tr>
      }
      </tbody>
    );
  }
}

export default ClosestDomainsItem;
