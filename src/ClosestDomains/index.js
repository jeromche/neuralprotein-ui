import React, { Component } from 'react';
import { observer } from 'mobx-react';
import ClosestDomainsItem from '../components/ClosestDomainsItem';

@observer(['neighbors'])
export default class ClosestDomains extends Component {
  render() {
    const metric = this.props.neighbors.metric;
    const nearestDomains = this.props.neighbors[`${metric}Domains`];
    const ClosestDomainsItems = nearestDomains.map((item, i) => (
      <ClosestDomainsItem domain={item} key={i} />
    ));

    return (
      <section>
        <table className="table table-hover table-sm">
          <thead className="thead-default">
            <tr>
              <th scope="col">Domain ID</th>
              <th scope="col">Name</th>
              <th scope="col">Classification</th>
              <th scope="col">&nbsp;</th>
            </tr>
          </thead>
          <tfoot></tfoot>
          {ClosestDomainsItems}
        </table>
      </section>
    );
  }
}
