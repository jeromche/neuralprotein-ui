import React, { Component } from 'react';

export default class Criteria extends Component {
  onChange = (event) => {
    const data = {};
    data[event.target.name] = event.target.value;
    this.props.neighbors.setAdvanced(data);
  }

  onSubmit = (event) => {
    event.preventDefault();
    this.props.neighbors.getNeighbors();
  }

  render = () => {
    const { neighbors } = this.props;

    return (
      <section>
        <h4>Structural Neighbors Criteria</h4>

        <form onSubmit={this.onSubmit}>
          <div className="row">
            <div className="col-sm-4">
              <fieldset className="form-group">
                <label
                  className="col-sm-6 form-control-label"
                  htmlFor="upload-num-neighbors">
                  Num. neighbors
                </label>

                <div className="col-sm-6">
                  <input
                    id="upload-num-neighbors"
                    name="numNeighbors"
                    type="number"
                    min="0"
                    max="100"
                    step="1"
                    onChange={this.onChange}
                    defaultValue={neighbors.numNeighbors}
                    className="form-control"/>
                </div>
              </fieldset>
            </div>
            <div className="col-sm-6">
              <fieldset className="form-group">
                <label
                  className="col-sm-2 form-control-label"
                  htmlFor="upload-metric">
                  Metric {neighbors.metric}
                </label>

                <div className="col-sm-10">
                  <select
                    id="upload-metric"
                    onChange={this.onChange}
                    className="form-control"
                    defaultValue={neighbors.metric}>
                    <option value="nearest">SCOP Likelyhood</option>
                    <option value="closest">Euclidean distance</option>
                  </select>
                </div>
              </fieldset>
            </div>
            <div className="col-sm-2">
              {this.props.neighbors.embedding2D.x !== undefined && (
                <input type="submit" value="Get Neighbors" className="btn btn-secondary"/>
              )}
            </div>
          </div>
        </form>
      </section>
    )
  }
}
