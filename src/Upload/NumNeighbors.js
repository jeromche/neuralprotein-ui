import React, { Component } from 'react';
import { observer } from 'mobx-react';

@observer(['neighbors'])
class Upload extends Component {
  componentWillMount() {
    this.setState({ numNeighbors: this.props.neighbors.numNeighbors });
  }

  /**
   * Sets the `numNeighbors` in this component's local state.
   * @param {Object} event Change event of the text input.
   */
  setNumNeighbors(event) {
    let value = parseInt(event.target.value, 10);
    this.setState({ numNeighbors: value });
  }

  /**
   * Saves the local state `numNeighbors` values to the data neighbors.
   */
  saveNumNeighbors(event) {
    event.preventDefault();
    this.props.neighbors.setNumNeighbors(this.state.numNeighbors);
    this.props.neighbors.upload();
  }

  render() {
    return (
      <form onSubmit={this.saveNumNeighbors.bind(this)}>
        <fieldset className="form-group row">
          <label
            className="col-sm-2 form-control-label"
            htmlFor="upload-num-neighbors">
            Num. neighbors
          </label>

          <div className="col-sm-2">
            <input
              id="upload-num-neighbors"
              type="number"
              min="0"
              max="100"
              step="1"
              onChange={this.setNumNeighbors.bind(this)}
              defaultValue={this.state.numNeighbors}
              className="form-control"/>
          </div>

          <div className="col-sm-2">
            <button
              type="button"
              className="btn btn-primary m-r-1"
              onClick={this.saveNumNeighbors.bind(this)}>
              Save
            </button>
          </div>
        </fieldset>
      </form>
    )
  }
}

export default Upload;
