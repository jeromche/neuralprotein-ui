import React, { Component } from 'react';
import { observer } from 'mobx-react';
import FileInput from './FileInput.js';
import LoadingIndicator from './LoadingIndicator';
import Display from './Display.js';
import Neighbors from '../Neighbors';
import Criteria from '../Criteria';

@observer(['neighbors'])
export default class Upload extends Component {

  edit = () => {
    const neighbors = this.props.neighbors;
    neighbors.setShowUploadForm(!neighbors.showUploadForm);
  }

  toggleAdvanced = () => {
    const neighbors = this.props.neighbors;
    const show = !neighbors.showAdvanced;

    neighbors.setShowAdvanced(show);
  }

  render() {
    const { isUploading, showUploadForm } = this.props.neighbors;

    return (
      <section>
        {isUploading
          ? <LoadingIndicator/>
          : (
            <div>
              {showUploadForm
                ? <FileInput edit={this.edit} advanced={this.toggleAdvanced}/>
                : <Display edit={this.edit}/>
              }

              <hr/>
              <Criteria neighbors={this.props.neighbors}/>
              <hr/>
              {this.props.neighbors.embedding2D.x !== undefined && <Neighbors/>}
            </div>
          )
        }
      </section>
    )
  }
}
