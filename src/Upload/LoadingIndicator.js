import React from 'react'

const LoadingIndicator = () => (
  <div className="row">
    <div className="col-md-12">
      <h4>Uploading&hellip;</h4>
      <p>Please hang on.</p>
    </div>

  </div>
)

export default LoadingIndicator
