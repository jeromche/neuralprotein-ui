import React, { Component } from 'react';
import { observer } from 'mobx-react';
import Advanced from '../Advanced';
import './style.css';

@observer(['neighbors'])
export default class FileInput extends Component {
  constructor() {
    super();
    this.state = { file: undefined };
  }

  setFile(event) {
    this.setState({ file: event.target.files[0] });
  }

  uploadFile(event) {
    event.preventDefault();

    this.props.neighbors.setFile(this.state.file);
    this.props.neighbors.upload();
  }

  render() {
    const { isUploading } = this.props.neighbors;

    return (
      <form className="upload-file-input" onSubmit={this.uploadFile.bind(this)}>
        {this.props.neighbors.showUploadError && (
          <div className="alert alert-danger" role="alert">
            <strong>Oh snap!</strong> Change a few things up and try submitting again.
          </div>
        )}

        <fieldset className="row">
          <div className="form-group col-sm-4">
            <h1>PDB</h1>

            <input
              type="file"
              id="upload-file"
              className="form-control"
              onChange={this.setFile.bind(this)}
            />

          </div>

          <div className="col-sm-8">
            {!isUploading && ( <Advanced advanced={this.toggleAdvanced}/> )}
          </div>
        </fieldset>


        {this.props.neighbors.embedding2D.x !== undefined && (
          <div>
            <hr/>
            <nav className="btn-group">
              <button
                type="button"
                onClick={this.props.edit}
                className="btn btn-secondary">
                Hide Settings
              </button>
            </nav>
          </div>
        )}
      </form>
    )
  }
}
