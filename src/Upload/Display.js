import React, { Component } from 'react';
import { observer } from 'mobx-react';

@observer(['neighbors'])
export default class Display extends Component {
  render() {
    return (
      <div>
        <dl className="row">
          <dt className="col-sm-2">Filename:</dt>
          <dd className="col-sm-10"><code>{this.props.neighbors.fileName}</code></dd>
        </dl>

        <dl className="row">
          <dt className="col-sm-2">Vector:</dt>
          <dd className="col-sm-10 fold-space-vector">
            <code>{this.props.neighbors.foldSpaceVector.join(',')}</code>
          </dd>
        </dl>

        <nav>
          <button type="button" onClick={this.props.edit} className="btn btn-secondary">
            Show Settings
          </button>
        </nav>
      </div>
    )
  }
};
