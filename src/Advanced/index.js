import React, { Component } from 'react'
import { observer } from 'mobx-react';

@observer(['neighbors'])
export default class Advanced extends Component {
  onChange(event) {
    const data = {};
    data[event.target.name] = event.target.value;
    this.props.neighbors.setAdvanced(data);
  }

  render = () => (
    <div>
      <fieldset className="form-group">
        <label htmlFor="upload-chain" className="col-sm-2">Chain</label>
        <div className="col-sm-10">
          <input
            id="upload-chain"
            type="text"
            className="form-control"
            name="chain"
            defaultValue={this.props.neighbors.chain}
            onChange={this.onChange.bind(this)}/>
        </div>
      </fieldset>

      <fieldset className="form-group">
        <label htmlFor="upload-segment" className="col-sm-2">Segments</label>
        <div className="col-sm-10">
          <textarea
            id="upload-segment"
            rows="3"
            className="form-control"
            name="segment"
            defaultValue={this.props.neighbors.segment}
            onChange={this.onChange.bind(this)}
          />
        </div>
      </fieldset>

      <fieldset className="form-group">
        <label htmlFor="upload-distance-cut-off" className="col-sm-2">
          Distance Cut-off
        </label>
        <div className="col-sm-5">
          <input
            type="number"
            min="0"
            max="100"
            step="0.01"
            className="form-control"
            name="distanceCutOff"
            defaultValue={this.props.neighbors.distanceCutOff}
            onChange={this.onChange.bind(this)}
          />
          <small className="text-muted">Floating Point Number</small>
        </div>
        <div className="col-sm-5">
          <button type="submit" className="btn btn-primary">Upload</button>
        </div>
      </fieldset>
    </div>
  )
}
