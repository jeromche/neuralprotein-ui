import React from 'react'
import { Link } from 'react-router'
import './styles.css'

const HomePage = () => (
  <nav className="homeNav">
    <div className="row">
      <div className="col-sm-6">
        <Link to="/neighbor-finder">Protein Neighbor Finder</Link>
      </div>

      <div className="col-sm-6">
        <a href="#" className="disabledLink">Secondary Structure Prediction</a>
      </div>
    </div>

    <div className="row">
      <div className="col-sm-6">
        <a href="#" className="disabledLink">Fold Recognition</a>
      </div>

      <div className="col-sm-6">
        <a href="#" className="disabledLink">Automatic Classification</a>
      </div>
    </div>
  </nav>
)

export default HomePage
