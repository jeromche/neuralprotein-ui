import axios from 'axios';

let instance = undefined;

//const baseURL = 'http://coltrane.fisica.unam.mx:5000/api/v1';
const baseURL = 'http://52.53.186.152:5000/api/v1';

if (instance === undefined) {
  instance = axios.create({ baseURL });
}

export default instance;
