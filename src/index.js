import React from 'react';
import ReactDOM from 'react-dom';

import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { Provider } from 'mobx-react';

import App from './App/index.js';
import HomePage from './HomePage/index.js';
import Upload from './Upload/index.js';
import Neighbors from './Neighbors/index.js';
import NeighborStore from './stores/neighbors.js';
import CathStore from './stores/cath.js';

import 'bootstrap/dist/css/bootstrap.css';

const rootEl = document.getElementById('root')
const neighborStore = new NeighborStore();
const cathStore = new CathStore();

ReactDOM.render((
  <Provider neighbors={neighborStore} cath={cathStore}>
    <Router history={browserHistory}>
      <Route path="/" component={App}>
        <IndexRoute component={HomePage} />
        <Route path="neighbor-finder" component={Upload}/>
        <Route path="neighbors" component={Neighbors}/>
      </Route>
    </Router>
  </Provider>
), rootEl);
