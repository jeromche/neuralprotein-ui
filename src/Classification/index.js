import React, { Component } from 'react';
import { observer } from 'mobx-react';
import ProbaTr from '../components/ProbaTr';

@observer(['neighbors'])
export default class Classification extends Component {
  render() {
    const probabilities = this.props.neighbors.probabilities;
    const probaTrs = Object.keys(probabilities).map((key, i) => (
      <ProbaTr class={key} proba={probabilities[key]} key={i} />
    ));

    return (
      <section>
        <h4>Filename: <code>{this.props.neighbors.fileName}</code></h4>

        <table className="table table-striped table-hover table-sm">
          <thead className="thead-default">
            <tr>
              <th scope="col">Predicted Class</th>
              <th scope="col">Score</th>
            </tr>
          </thead>
          <tfoot></tfoot>
          <tbody>{probaTrs}</tbody>
        </table>
      </section>
    )
  }
}
