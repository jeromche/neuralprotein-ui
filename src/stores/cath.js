import { observable, action, useStrict } from 'mobx';
import axios from '../axios.js';

useStrict(true);

export default class CathStore {
  spaceRepresentationURL = '/space_representation/CATH';

  api = undefined;
  storage = undefined;

  @observable isLoading;
  @observable mapping;
  @observable positions2d;

  constructor() {
    this.setInitialValues();
    this.getSpaceRepresentation();
  }

  @action('Set initial values.')
  setInitialValues() {
    this.isLoading = true;
    this.mapping = {};
    this.positions2d = [];
  }

  getSpaceRepresentation() {
    let local = JSON.parse(window.localStorage.getItem('neuro-spaceRepresentation'));
    if (false && local !== null) {
      this.setSpaceRepresentation(local);
      return;
    }

    axios.get(this.spaceRepresentationURL).then(({ data }) => {
      this.setSpaceRepresentation(data);
      this.saveToLocalStorage(data);
    });
  }

  @action('Set space representation.')
  setSpaceRepresentation(spaceRepresentation) {
    this.mapping = spaceRepresentation.mapping;
    this.positions2d = spaceRepresentation.positions2d;
    this.isLoading = false;
  }

  saveToLocalStorage(spaceRepresentation) {
    const data = JSON.stringify(spaceRepresentation);
    window.localStorage.setItem('neuro-spaceRepresentation', data);
  }
}
