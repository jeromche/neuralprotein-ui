import { observable, action, useStrict } from 'mobx';
import Axios from 'axios';
import axios from '../axios.js';

useStrict(true);

export default class NeighborStore {
  api = undefined;
  storage = undefined;
  file = undefined;

  uploadData = undefined;

  // Flags
  @observable isUploading;
  @observable showUploadForm;
  @observable showUploadError;
  @observable showAdvanced;

  // Data from API
  @observable foldSpaceVector;
  @observable fileName;
  @observable numNeighbors;
  @observable metric;
  @observable chain;
  @observable segment;
  @observable distanceCutOff;
  @observable classificationLevel;
  @observable probabilities;
  @observable closestDomains;
  @observable nearestDomains;
  @observable embedding2D;

  /**
   * @todo Find out how to set default value.
   * @type {Object}
   */
  constructor() {
    this.setInitialValues();
  }

  saveToLocalStorage() {
    const rawData = {
      fileName: this.fileName,
      foldSpaceVector: this.foldSpaceVector,
      probabilities: this.probabilities,
      numNeighbors: this.numNeighbors,
      chain: this.chain,
      distanceCutOff: this.distanceCutOff,
      classificationLevel: this.classificationLevel,
      embedding2D: this.embedding2D,
      nearestDomains: this.nearestDomains,
      metric: this.metric,
    }

    const data = JSON.stringify(rawData);

    window.localStorage.setItem('neuro', data);
  }

  getFromLocalStorage() {
    const local = JSON.parse(window.localStorage.getItem('neuro'));

    if (local === null) return false;

    for (let key in local) if (true) {
      this[key] = local[key];
    }

    return true;
  }

  @action('Set initialial values.')
  setInitialValues() {
    this.isUploading = false;
    this.showUploadForm = true;
    this.showAdvanced = false;

    if (this.getFromLocalStorage()) {
      this.showUploadForm = this.fileName === '';
      return;
    }

    this.fileName = '';
    this.foldSpaceVector = [];
    this.probabilities = {};
    this.numNeighbors = 15;
    this.metric = 'closest';
    this.chain = 'A';
    this.distanceCutOff = 5;
    this.classificationLevel = 'C';
    this.embedding2D = { x: undefined, y: undefined };
    this.nearestDomains = [];
    this.cloestDomains = [];

    this.saveToLocalStorage();
  }

  setFile(file) {
    this.file = file;

    const data = new FormData();
    data.append('file', file);
    this.uploadData = data;
  }

  upload() {
    this.setIsUploading(true);
    this.setShowUploadError(false);

    console.log('upload');
    console.log(this.metric);

    axios
      .post('/pdb/fold_space_vector/', this.uploadData)
      .then(this.setFoldSpaceVector.bind(this))
      .then(() => { this.setFileName(this.file.name) })
      .then(this.getNeighbors.bind(this))
      .catch((error) => {
        this.setIsUploading(false);
        this.setShowUploadForm(true);
        this.setShowUploadError(true);
      });

    return axios;
  }

  @action('Set status of isUploading flag.')
  setIsUploading(bool) {
    this.isUploading = bool;
  }

  @action('Set status of showUploadForm flag.')
  setShowUploadForm(bool) {
    this.showUploadForm = bool;

    if (bool === false) {
      this.showAdvanced = false;
    }
  }

  @action('Set status of showUploadError flag.')
  setShowUploadError(bool) {
    this.showUploadError = bool;
  }

  @action('Set status of showAdvanced flag')
  setShowAdvanced(bool) {
    this.showAdvanced = bool;

    if (bool) {
      this.showUploadForm = true;
    }
  }

  /**
   * Fires various API requests simultaneously after receiving the fold space
   * vector.
   */
  getNeighbors() {
    const classificationLevel = this.classificationLevel;
    const numNeighbors = this.numNeighbors;
    const options = { foldSpaceVector: this.foldSpaceVector };
    const metric = this.metric || 'nearest';

    const getClassification = () => {
      const url = `vector/CATH/protein_structure_classification/proba/${classificationLevel}/`;
      return axios.post(url, options);
    }

    const getEmbedding = () => {
      return axios.post('vector/embedding2D/', options);
    }

    const getNearestDomains = () => {
      return axios.post(`vector/SCOP/${metric}_domains/full/${numNeighbors}/`, options);
    }

    Axios
      .all([
        getClassification(),
        getEmbedding(),
        getNearestDomains()
      ])
      .then(Axios.spread(this.setNeighbors.bind(this)))
      .then(() => {
        this.setIsUploading(false);
        this.setShowUploadForm(false);
      })
      .catch(() => {
        this.setIsUploading(false)
        this.setShowUploadForm(true)
      });
  }

  /**
   * Sets the values returned by the various API requests.
   */
  @action('Set neighbors.');
  setNeighbors = (classification, embedding, domains) => {
    const label = `${this.metric}Domains`;

    this.embedding2D = embedding.data.embedding2D;
    this[label] = domains.data[label];
    this.setClassification(classification.data);
    this.saveToLocalStorage();
  }

  @action('Set classification');
  setClassification(data) {
    let length = data.classes.length;
    for (let i = 0; i < length; i++) {
      this.probabilities[data.classes[i]] = data.probas[i];
    }
  }

  @action('Set filename.')
  setFileName = (fileName) => {
    this.fileName = fileName;
  }

  @action('Set foldspace vector.')
  setFoldSpaceVector(response) {
    this.foldSpaceVector = response.data.foldSpaceVector;
  }

  @action('Set structural neighbors criteria.')
  setCriteria = (numNeighbors, metric) => {
    this.numNeighbors = numNeighbors
    this.metric = metric;
    this.getNeighbors();
  }

  @action('Set advanced options.')
  setAdvanced = (data) => {
    for (let key in data) if (true) {
      this[key] = data[key];
    }
  }
}
