import React from 'react'
import Classification from '../Classification';
import ScatterPlot from '../ScatterPlot';
import ClosestDomains from '../ClosestDomains';

const Neighbors = () => (
  <div>
    <div className="row">
      <div className="col-md-6"><Classification/></div>
      <div className="col-md-6"><ScatterPlot/></div>
    </div>
    <div className="row">
      <div className="col-md-12"><ClosestDomains/></div>
    </div>
  </div>
)

export default Neighbors
