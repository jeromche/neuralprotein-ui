import React from 'react'
import NavLink from '../components/NavLink.js'
import './styles.css'

const App = (props) => (
  <div>
    <ul className="nav nav-tabs">
      <NavLink to="/">Home</NavLink>
      <NavLink to="/neighbor-finder">Neighbor Finder</NavLink>
    </ul>

    <div className="App-container">{props.children}</div>
  </div>
)

export default App
